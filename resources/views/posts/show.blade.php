<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <!-- Styles -->
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet"/>
    <!-- font-awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- CSS custom -->
    <link href="{{ url('/css/style.css') }}" rel="stylesheet"/>
    <title>Blog</title>
  </head>
  <body>
    <div class="container">

      <h1>Publicaciones</h1>

        <div class="card mb-4">
          <div class="card-header">
            <h3 class="card-title">{{$post->title}}</h3>
          </div>
          <div class="card-body">
            <p class="card-text">{{$post->body}}</p>
            <a href="{{ url('/posts') }}">Regresar</a>
          </div>
        </div>

          <!-- Lista de Comentarios -->


          <div class="d-flex justify-content-center">


          <div class="card">
              <div class="card-body">
                  <h4 class="card-title">Comentarios</h4>
                  <h6 class="card-subtitle">Los más recientes comentarios de los usuarios</h6>
              </div>
              <div class="comment-widgets m-b-20">
                @foreach ($post->comments as $comment)
                  <div class="d-flex flex-column comment-row">

                      <div class="p-2 w-100">
                        <span class="round">
                          <img src="https://i.imgur.com/uIgDDDd.jpg" alt="user" width="50">
                          <h3>{{$comment->email}}</h3>
                        </span>
                      </div>

                      <div class="comment-text w-100">
                          <h5>{{$comment->name}}</h5>
                          <div class="comment-footer"> <span class="date">April 14, 2019</span> <span class="label label-info">Pending</span> <span class="action-icons"> <a href="#" data-abc="true"><i class="fa fa-pencil"></i></a> <a href="#" data-abc="true"><i class="fa fa-rotate-right"></i></a> <a href="#" data-abc="true"><i class="fa fa-heart"></i></a> </span> </div>
                          <p class="m-b-5 m-t-10">{{$comment->body}}</p>
                      </div>
                  </div>
                @endforeach
            </div>
          </div>

        </div>




    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
  </body>
</html>
