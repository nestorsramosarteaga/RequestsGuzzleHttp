<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <!-- Styles -->
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet"/>
    <!-- font-awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- CSS custom -->    
    <link href="{{ url('/css/style.css') }}" rel="stylesheet"/>
    <title>Blog</title>
  </head>
  <body>
    <div class="container">
      <h1>Publicaciones</h1>
      @foreach($posts as $post)
        <div class="card mb-4">
          <div class="card-header">
            <a href="{{ url('/posts/' . $post->id) }}"><h3 class="card-title">{{$post->title}}</h3></a>
          </div>
          <div class="card-body">
            <p class="card-text">{{$post->body}}</p>
          </div>
        </div>
      @endforeach
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
  </body>
</html>
