<?php

namespace App\Http\Controllers;

use App\Repositories\Posts;
use App\Repositories\Comments;

use Illuminate\Http\Request;

use GuzzleHttp\Client;

class PostsController extends Controller
{
    protected $posts;

    public function __construct(Posts $posts)
    {
      $this->posts = $posts;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $posts = $this->posts->all();
      return view('posts.index',compact('posts'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

      $post = $this->posts->find($id);

      $post->comments= $this->posts->comments($post->id);
      
      return view('posts.show',compact('post'));
    }

}
