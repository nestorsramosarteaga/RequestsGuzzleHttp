<?php

namespace App\Providers;

use GuzzleHttp\Client;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      $this->app->singleton('GuzzleHttp\Client', function(){
        return new Client([
          'base_uri' => 'https://jsonplaceholder.typicode.com',
          //'timeout'  => 12.0,
          'auth' => ['username', 'password'],
          'headers' => [
              'User-Agent' => 'testing/1.0',
              'Accept'     => 'application/json',
              'X-Foo'      => ['Bar', 'Baz']
          ],
        ]);
      });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
