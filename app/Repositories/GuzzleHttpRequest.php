<?php
namespace App\Repositories;

use GuzzleHttp\Client;

class GuzzleHttpRequest
{
  protected $client;

  public function __construct(Client $client)
  {
    //Retornado del método register() en AppServiceProvider
    $this->client = $client;
  }

  protected function get($url)
  {
    $response = $this->client->request('GET', $url);

    return json_decode($response->getBody()->getContents());
  }

  protected function post($url){
    return true;
  }

}
